#include <cassert>
#include <array>
#include <qtools/lattice/square.h>

int main()
{
  const auto LatNm  = qtools::lattice::name::square;
  const auto length = std::array<int,2>{{100,100}};
  assert(qtools::lattice::info<LatNm>::dimension == 2);
  assert(qtools::lattice::info<LatNm>::max_num_outgoing_edges_per_vertex == 2);
  assert(qtools::lattice::info<LatNm>::num_vertices(length) == 10000);
  assert(qtools::lattice::info<LatNm>::num_edges(length) == 20000);
  assert(qtools::lattice::info<LatNm>::max_num_outgoing_edges(length) == 20000);

  int sum;
  const auto addindex = [length,&sum](const int x, const int y)
                        { sum += x + length[0] * y; };

  sum = 0;
  qtools::lattice::info<LatNm>::for_each_vertex(length, addindex);
  assert(sum == 49995000);

  sum = 0;
  qtools::lattice::info<LatNm>::for_each_sublattice_vertex(length, addindex);
  assert(sum == 49995000);

  return 0;
}
