#include <cassert>
#include <string>
#include <qtools/argument.h>

int main(int argc, char** argv)
{
  std::string name = qtools::fetch_argument_string(argc, argv, 0, "abc");
  assert(name == std::string("./../bin/argument_test"));
  return 0;
}
