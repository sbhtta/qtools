#include <cassert>
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <qtools/databook.h>

double mymean(std::vector<double>& v)
{
  double sum = 0.;
  for (int i = 0; i < (int)v.size(); i++) sum += v[i];
  return sum / (1. * v.size());
}

enum class observable
{
  position,
  velocity,
  density,
  all,
};

void test_enum_multiple_blocks()
{
  auto db = qtools::databook<double,3,observable>(100,5); 
  db.set_maxlag(10);

  for (int t = 0; t < 100; t++) {
    db.add_data(observable::position, 2 * t);
    db.add_data(observable::velocity, 3 * t + 5);
    db.add_data(observable::density, t * t);
  }
  assert(std::fabs(mymean(db.data(observable::position, 0)) - 19.) < 1e-4);
  assert(std::fabs(db.autocorrelation(observable::density)(0) - 1.) < 1e-4);

  db.clear();
  for (int t = 0; t < 100; t++) {
    db.add_data(0, t);
    db.add_data(1, 3 * t);
    db.add_data(2, t * t + 5);
  }
  assert(std::fabs(mymean(db.data(0,0)) - 9.5) < 1e-4);
  assert(std::fabs(db.autocorrelation(0)(0) - 1.) < 1e-4);

  std::string filename("databook_test_output.tmp");
  std::ofstream myfile(filename);
  myfile << db << std::endl;

  db.clear();
  std::ifstream infile(filename);
  infile >> db;
  assert(std::fabs(mymean(db.data(0,0)) - 9.5) < 1e-4);

  std::remove(filename.c_str());
  return;
}

void test_single_block()
{
  auto db = qtools::databook<double,3>(100); 
  db.set_maxlag(10);

  for (int t = 0; t < 100; t++) {
    db.add_data(0, t);
    db.add_data(1, 3 * t);
    db.add_data(2, t * t + 5);
  }
  assert(std::fabs(mymean(db.data(0)) - 49.5) < 1e-4);
  assert(std::fabs(db.autocorrelation(0)(0) - 1.) < 1e-4);

  std::string filename("databook_test_output.tmp");
  std::ofstream myfile(filename);
  myfile << db << std::endl;

  db.clear();
  std::ifstream infile(filename);
  infile >> db;
  assert(std::fabs(mymean(db.data(0)) - 49.5) < 1e-4);

  std::remove(filename.c_str());
  return;
}


int main()
{
  test_enum_multiple_blocks();
  test_single_block();
  return 0;
}
