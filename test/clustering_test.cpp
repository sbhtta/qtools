#include <cassert>
#include <vector>
#include <iostream>
#include <qtools/connected_components.h>

void test_1Dimage()
{
  auto C = qtools::connected_components();
  C.resize(100);
  C.initialize();
  for (int i = 0; i < C.size()-1; i++) {
    C.merge_roots(i,i+1);
  }
  C.finalize();
  assert(C.largest_component_size() == 100);

  C.resize(50);
  assert(C.size() == 50);
  assert(C.capacity() == 100);
  C.initialize();
  for (int i = 0; i < C.size()-1; i++) {
    C.atomic_merge_roots(i,i+1);
  }
  C.finalize();
  assert(C.largest_component_size() == 50);

  return;
}

void test_2Dimage()
{
  // create an image with integer valued pixels:
  // 5 5 5 5 5
  // 5 6 5 6 5
  // 5 5 5 5 5
  // 5 6 5 6 5
  // 5 5 5 5 5
  const int L = 5;
  std::vector<int> image(L * L, 0);
  for (int y = 0; y < L; y++) {
    for (int x = 0; x < L; x++) {
      image[x + L * y] = 5 + ((x & 1) && (y & 1));
    }
  }

  // form and label components with neighbor
  // pixels which have the same value,
  // values of component labels are only indicative:
  // 0 0 0 0 0
  // 0 2 0 4 0
  // 0 0 0 0 0
  // 0 3 0 1 0
  // 0 0 0 0 0
  auto C = qtools::connected_components();
  C.resize(image.size());
  C.initialize();
  for (int y = 0; y < L; y++) {
    for (int x = 0; x < L; x++) {
      auto center = (x                     ) + L * (y                     );
      auto left   = (x == 0 ? L - 1 : x - 1) + L * (y                     );
      auto right  = (x == L - 1 ? 0 : x + 1) + L * (y                     );
      auto bottom = (x                     ) + L * (y == 0 ? L - 1 : y - 1);
      auto top    = (x                     ) + L * (y == L - 1 ? 0 : y + 1);

      if (image[center] == image[left  ]) C.merge_roots(center, left  );
      if (image[center] == image[right ]) C.merge_roots(center, right );
      if (image[center] == image[bottom]) C.merge_roots(center, bottom);
      if (image[center] == image[top   ]) C.merge_roots(center, top   );
    }
  }
  C.finalize();

  assert(C.largest_component_size() == 21);
  return;
}

int main()
{
  test_1Dimage();
  test_2Dimage();
  return 0;
}
