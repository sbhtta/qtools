#include <cassert>

#ifdef WITH_OPENMP
#include <omp.h>
#endif

#include <qtools/atomic.h>

void atomic_add_test()
{
  #ifdef WITH_OPENMP
  int n = omp_get_max_threads();
  int expected = (n * (n - 1)) / 2;
  int sum = 0;
  #pragma omp parallel
  {
    qtools::atomic_add(&sum, (int)omp_get_thread_num());
  }
  assert(expected == sum);
  #else
  int n = 10;
  int expected = (n * (n - 1)) / 2;
  int sum = 0;
  for (int i = 0; i < n; i++) {
    qtools::atomic_add(&sum, i);
  }
  assert(expected == sum);
  #endif
  return;
}

int main()
{
  atomic_add_test();
}
