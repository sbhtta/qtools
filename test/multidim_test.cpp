#include <array>
#include <cassert>
#include <qtools/multidim.h>

int main()
{
  const auto length = qtools::uniform_array<int,2>(10);

  std::array<int,2> tmp = {{10,10}};
  assert(length == tmp);

  assert(11 == qtools::flatten_coords<2>(length,1,1));
  assert(11 == qtools::flatten_wrap_coords<2>(length,11,11));

  assert((true == qtools::disp_crosses_face<2,+1,+0>(length,9,4)));
  return 0;
}
