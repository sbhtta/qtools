#include <cassert>
#include <array>
#include <qtools/lattice/hypercube.h>

int main()
{
  const auto LatNm  = qtools::lattice::name::hypercube;
  const auto length = std::array<int,4>{{10,10,10,10}};
  assert(qtools::lattice::info<LatNm>::dimension == 4);
  assert(qtools::lattice::info<LatNm>::max_num_outgoing_edges_per_vertex == 4);
  assert(qtools::lattice::info<LatNm>::num_vertices(length) == 10000);
  assert(qtools::lattice::info<LatNm>::num_edges(length) == 40000);
  assert(qtools::lattice::info<LatNm>::max_num_outgoing_edges(length) == 40000);

  int sum;
  const auto addindex = [length,&sum](const int x, const int y, const int z, const int t)
                        { sum += x + length[0] * (y + length[1] * (z + length[2] * t)); };

  sum = 0;
  qtools::lattice::info<LatNm>::for_each_vertex(length, addindex);
  assert(sum == 49995000);

  sum = 0;
  qtools::lattice::info<LatNm>::for_each_sublattice_vertex(length, addindex);
  assert(sum == 49995000);

  return 0;
}
