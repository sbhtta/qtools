#include <cassert>
#include <array>
#include <qtools/lattice/cube.h>

int main()
{
  const auto LatNm  = qtools::lattice::name::cube;
  const auto length = std::array<int,3>{{10,10,10}};
  assert(qtools::lattice::info<LatNm>::dimension == 3);
  assert(qtools::lattice::info<LatNm>::max_num_outgoing_edges_per_vertex == 3);
  assert(qtools::lattice::info<LatNm>::num_vertices(length) == 1000);
  assert(qtools::lattice::info<LatNm>::num_edges(length) == 3000);
  assert(qtools::lattice::info<LatNm>::max_num_outgoing_edges(length) == 3000);

  int sum;
  const auto addindex = [length,&sum](const int x, const int y, const int z)
                        { sum += x + length[0] * (y + length[1] * z); };

  sum = 0;
  qtools::lattice::info<LatNm>::for_each_vertex(length, addindex);
  assert(sum == 499500);

  sum = 0;
  qtools::lattice::info<LatNm>::for_each_sublattice_vertex(length, addindex);
  assert(sum == 499500);

  return 0;
}
