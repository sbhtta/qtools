#include <cassert>
#include <array>
#include <qtools/lattice/line.h>

int main()
{
  const auto LatNm  = qtools::lattice::name::line;
  const auto length = std::array<int,1>{{100}};
  assert(qtools::lattice::info<LatNm>::dimension == 1);
  assert(qtools::lattice::info<LatNm>::max_num_outgoing_edges_per_vertex == 1);
  assert(qtools::lattice::info<LatNm>::num_vertices(length) == 100);
  assert(qtools::lattice::info<LatNm>::num_edges(length) == 100);
  assert(qtools::lattice::info<LatNm>::max_num_outgoing_edges(length) == 100);

  int sum;
  const auto addindex = [length,&sum](const int x)
                        { sum += x; };

  sum = 0;
  qtools::lattice::info<LatNm>::for_each_vertex(length, addindex);
  assert(sum == 4950);

  sum = 0;
  qtools::lattice::info<LatNm>::for_each_sublattice_vertex(length, addindex);
  assert(sum == 4950);

  return 0;
}
