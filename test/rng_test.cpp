#include <cassert>
#include <qtools/rng.h>

int main()
{
  qtools::rng_vector rng_states(2);
  auto rng_copy = rng_states[0];
  auto value = qtools::rng_uniform(rng_copy);
  assert((-0.1 < value) && (value < 1.1));
  rng_states[0] = rng_copy;
  return 0;
}
