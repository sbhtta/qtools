#include <cassert>
#include <vector>
#include <qtools/statistics.h>

int main()
{
  std::vector<double> data {0,1,2,3,4,5,6,7,8,9};
  assert(std::fabs(qtools::mean(data)                       - 4.500000) < 1e-4);
  assert(std::fabs(qtools::variance(data)                   - 9.166666) < 1e-4);
  assert(std::fabs(qtools::standard_deviation(data)         - 3.027650) < 1e-4);
  assert(std::fabs(qtools::sample_moment<0>(data,0)         - 1.000000) < 1e-4);
  assert(std::fabs(qtools::sample_moment<1>(data,0)         - 4.500000) < 1e-4);
  assert(std::fabs(qtools::sample_moment<1>(data,4.5)       - 0.000000) < 1e-4);
  assert(std::fabs(qtools::sample_moment<2>(data,0)         - 28.50000) < 1e-4);
  assert(std::fabs(qtools::sample_moment<2>(data,4.5)       - 8.250000) < 1e-4);
  assert(std::fabs(qtools::sample_moment<3>(data,0)         - 202.5000) < 1e-4);
  assert(std::fabs(qtools::sample_moment<3>(data,4.5)       - 0.000000) < 1e-4);
  assert(std::fabs(qtools::sample_moment<4>(data,0)         - 1533.300) < 1e-4);
  assert(std::fabs(qtools::sample_moment<4>(data,4.5)       - 120.8625) < 1e-4);

  std::array<double,5> displacement  {{3.2,5.1,6.5,8.9,7.3}};
  std::array<double,7> time_taken    {{4.3,9.0,8.1,3.2,2.2}};

  auto velocity = qtools::standard_mean_error(displacement)
                  / qtools::standard_mean_error(time_taken);
  assert(std::fabs(velocity.mean  - 1.619400) < 1e-4);
  assert(std::fabs(velocity.error - 0.628155) < 1e-4);

  auto velocity2 = qtools::jackknife_mean_error(displacement)
                   / qtools::jackknife_mean_error(time_taken);
  assert(std::fabs(velocity2.mean  - 1.619400) < 1e-4);
  assert(std::fabs(velocity2.error - 0.628155) < 1e-4);

  return 0;
}
