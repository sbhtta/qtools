#include <cassert>
#include <cmath>
#include <array>
#include <vector>
#include <qtools/lattice/correlator.h>

int main()
{
  const auto LatNm  = qtools::lattice::name::square;
  const auto length = std::array<int,2>{{100, 20}};
  const auto corr   = [](const double a, const double b)
                      { return (a == b); };
        auto data   = std::vector<double>(10000, 0.);
        auto C      = qtools::lattice::correlator<LatNm>(length);

  for (int y = 0; y < 100; y++) {
    for (int x = 0; x < 100; x++) {
      data[x + 100 * y] = (x & 1) + 2 * (y & 1);
    }
  }
  for (int t = 0; t < 10; t++) {
    for (int y = 0; y < 100; y++) {
      for (int x = 0; x < 100; x++) {
        data[x + 100 * y] = 4 - data[x + 100 * y];
      }
    }
    C.add_data(data.data(), corr);
  }

  assert(C.count() == 10);
  assert(std::fabs(C(0) - 1.) < 1e-4);
  assert(std::fabs(C(1) - 0.) < 1e-4);
  assert(std::fabs(C(2) - 1.) < 1e-4);
  assert(std::fabs(C(3) - 0.) < 1e-4);
  return 0;
}
