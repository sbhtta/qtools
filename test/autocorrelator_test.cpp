#include <cassert>
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>

#include <qtools/autocorrelator.h>

struct myvec
{
  myvec(){;}
  myvec(const double ivx, const double ivy)
  { vx = ivx; vy = ivy; }
  double vx = 0.;
  double vy = 0.;
};

myvec
operator+(const myvec lhs, const myvec rhs)
{ return myvec(lhs.vx + rhs.vx, lhs.vy + rhs.vy); }

myvec&
operator+=(myvec& lhs, const myvec rhs)
{
  lhs.vx += rhs.vx;
  lhs.vy += rhs.vy;
  return lhs;
}

myvec
operator-(const myvec lhs, const myvec rhs)
{ return myvec(lhs.vx - rhs.vx, lhs.vy - rhs.vy); }

double
operator*(const myvec lhs, const myvec rhs)
{ return (lhs.vx * rhs.vx + lhs.vy * rhs.vy); }

myvec
operator*(const myvec lhs, const double rhs)
{ return myvec(lhs.vx * rhs, lhs.vy * rhs); }

myvec
operator*(const double lhs, const myvec rhs)
{ return myvec(lhs * rhs.vx, lhs * rhs.vy); }

myvec
operator/(const myvec lhs, const double rhs)
{ return myvec(lhs.vx / rhs, lhs.vy / rhs); }

void test_vector_data()
{
  qtools::autocorrelator<myvec> A(2);
  A.add_data(myvec(2,3));
  A.add_data(myvec(1.5,4));
  A.add_data(myvec(-1,2));
  A.add_data(myvec(0,9));
  return;
}

void test_scalar_data()
{
  auto A = qtools::autocorrelator<double>(3);
  A.add_data(2);
  A.add_data(3);
  A.add_data(4);
  A.add_data(3);
  A.add_data(5);
  A.add_data(6);
  A.add_data(4);
  A.add_data(3);

  assert(std::fabs(A(0) - 1.000000) < 1e-4);
  assert(std::fabs(A(1) - 0.277174) < 1e-4);
  assert(std::fabs(A(2) + 0.228261) < 1e-4);
  assert(std::fabs(A(3) + 0.016304) < 1e-4);
  assert(A.tau(3) > 0);

  std::string filename("autocorrelator_test_output.tmp");
  std::ofstream myfile(filename);
  myfile << A << std::endl;

  std::ifstream infile(filename);
  int t = 0, lag;
  double value;
  while (infile >> lag >> value) {
    assert(t == lag);
    assert(std::fabs(A(t) - value) < 1e-4);
    t++;
  }
  std::remove(filename.c_str());
  A.clear();

  return;
}

int main()
{
  test_scalar_data();
  test_vector_data();
  return 0;
}
