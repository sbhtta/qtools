#include <cassert>
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <qtools/histogram.h>

int main()
{
  auto h = qtools::histogram<2>({{-1.0,+1.0,0.1}},{{-1.0,+1.0,0.1}});
  h.add_data(0.35,0.35);
  h.add_data(0.75,0.35);
  h.add_data(-0.5,0.1);
  h.add_data(-1.0,1.0);

  double sum = 0.0;
  for (int y = 0; y < h.size(1); y++) {
  for (int x = 0; x < h.size(0); x++) {
    sum += h(x,y);
  }}
  assert(std::fabs(sum - 1.) < 1e-4);

  std::string filename("histogram_test_output.tmp");
  std::ofstream myfile(filename);
  myfile << h << std::endl;

  std::ifstream infile(filename);
  int i = 0, j = 0;
  double tmp1, tmp2, tmp3;
  while (infile >> tmp1 >> tmp2 >> tmp3) {                                                
    assert(std::fabs(h(i,j) - tmp3) < 1e-4);
    i++;
    if (i == h.size(0)) {
      i = 0;
      j++;
    }
  }
  std::remove(filename.c_str());
  h.clear();

}
