#!/bin/bash

## md2html: converts a directory full of .md files to .html files
## ==============================================================
## author: Soumyadeep Bhattacharya
## email: sbhtta@gmail.com
## last updated: 2015-10-30

if [[ "$1" == "-help" || "$#" < 2 || "#$" == 4 || "$#" > 6 ]]; then
  echo ""
  echo "md2html: converts .md files to .html files"
  echo "=========================================="
  echo ""
  echo "usage"
  echo "-----"
  echo "$0 cmd in out head.html foot.html index.md"
  echo " "
  echo "arguments"
  echo "---------"
  echo "<cmd>        : (a) pass the markdown perl script, or"
  echo "               (b) pass 'download' to download script from"
  echo "               http://daringfireball.net/projects/markdown/"
  echo "               or (c) pass 'clean' to delete .html files   "
  echo "               corresponding to all .md files in the 'in'  "
  echo "               directory                                   "
  echo "<in>         : input directory containing the .md files"
  echo "[out]        : output directory"
  echo "[head.html]  : header portion to be prepended to .html files"
  echo "[foot.html]  : footer portion to be appended to .html files"
  echo "[index.md]   : .md file to be converted to index.html"
  echo ""
  echo "summary"
  echo "-------"
  echo "in is copied to out and all .md files in out are converted"
  echo "to .html files, with the contents of header.html prepended"
  echo "and the contents of footer.html appended to the generated"
  echo ".html files; a README.md file can be used as index.html by"
  echo "begin passed as the last argument"
  echo ""
  exit 1
fi

## file and directory names:
INPUTDIR=$2
OUTPUTDIR=$3
HTMLHEADERFILE=$4
HTMLFOOTERFILE=$5
INDEXMDFILE=$6

if [[ $1 == "clean" ]]; then
  ## delete .html file for each .md file:
  command cd $INPUTDIR; JUMPDIR=`echo $?`
  if [[ $JUMPDIR == 0 ]]; then
    shopt -s nullglob globstar
    for FILE in **/*.md; do
      HTMLFILE=${FILE%.*}.html
      [ -f $HTMLFILE ] && rm $HTMLFILE
    done
  fi
else
  ## convert each .md file to .html file:
  mkdir -p $OUTPUTDIR
  rsync -az $INPUTDIR/ $OUTPUTDIR
  if [[ $# == 3 ]]; then
    DEFAULTHTMLHEADERFILE="default_header_md2.html"
    echo "<!DOCTYPE html>" > $DEFAULTHTMLHEADERFILE
    echo "<html>" >> $DEFAULTHTMLHEADERFILE
    echo "<title></title>" >> $DEFAULTHTMLHEADERFILE
    echo "<head></head>" >> $DEFAULTHTMLHEADERFILE
    echo "<body>" >> $DEFAULTHTMLHEADERFILE
    HTMLHEADERFILE=$DEFAULTHTMLHEADERFILE

    DEFAULTHTMLFOOTERFILE="default_footer_md2.html"
    echo "</body>" > $DEFAULTHTMLFOOTERFILE
    echo "</html>" >> $DEFAULTHTMLFOOTERFILE
    HTMLFOOTERFILE=$DEFAULTHTMLFOOTERFILE

    HTMLHEADER=`cat "$HTMLHEADERFILE"`
    HTMLFOOTER=`cat "$HTMLFOOTERFILE"`
    rm $DEFAULTHTMLHEADERFILE
    rm $DEFAULTHTMLFOOTERFILE
  else
    HTMLHEADER=`cat "$HTMLHEADERFILE"`
    HTMLFOOTER=`cat "$HTMLFOOTERFILE"`
    if [[ $# == 6 ]]; then
      cp $INDEXMDFILE $OUTPUTDIR/index.md
    fi
  fi
  MDSCRIPTFILE="tmp_md2html_Markdown.pl"
  if [[ $1 == "download" ]]; then
    echo ""
    wget http://daringfireball.net/projects/downloads/Markdown_1.0.1.zip
    echo ""
    unzip -oqq Markdown_1.0.1.zip
    cp Markdown_1.0.1/Markdown.pl $OUTPUTDIR/$MDSCRIPTFILE
    rm -rf Markdown_1.0.1
    rm -rf Markdown_1.0.1.zip
  else
    cp $1 $OUTPUTDIR/$MDSCRIPTFILE
  fi
  command cd $OUTPUTDIR; JUMPDIR=`echo $?`
  if [[ $JUMPDIR == 0 ]]; then
    shopt -s nullglob globstar
    for FILE in **/*.md; do
      HTMLFILE=${FILE%.*}.html
      HTMLBODYFILE="tmp.body.$FILE"

      ## for original markdown,
      perl $MDSCRIPTFILE --html4tags $FILE > $HTMLBODYFILE

      ## for github flavored markdown,
      ## create JSON text from .md content and send to github API:
      ## jq --slurp --raw-input '{"text": "\(.)", "mode": "markdown"}' < $FILE | curl --data @- https://api.github.com/markdown > $HTMLBODYFILE

      ## prepend header and append footer:
      echo "$HTMLHEADER" > $HTMLFILE
      cat "$HTMLBODYFILE" >> $HTMLFILE
      echo "$HTMLFOOTER" >> $HTMLFILE
      rm $HTMLBODYFILE
    done
    ## delete copy of markdown script:
    [ -f $MDSCRIPTFILE ] && rm $MDSCRIPTFILE
  fi
fi
