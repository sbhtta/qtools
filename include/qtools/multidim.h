#ifndef QTOOLS_MULTIDIM_H
#define QTOOLS_MULTIDIM_H

#include <array>
#include <cassert>

namespace qtools {

template <typename T, std::size_t D>
std::array<T,D>
uniform_array(const T a)
{
  std::array<T,D> length;
  length.fill(a);
  return length;
}

template <std::size_t D, std::size_t N = D>
inline int
flatten_coords(const std::array<int,N> length,
               const int x0,
               const int x1 = 0,
               const int x2 = 0,
               const int x3 = 0)
{
  static_assert(D <= 4, "D > 4 not supported yet!");
  assert(D <= N);

  switch(D) {
  case 0 : return 0;
  case 1 : return x0;
  case 2 : return x0 + length[0] * x1;
  case 3 : return x0 + length[0] * (x1 + length[1] * x2);
  case 4 : return x0 + length[0] * (x1 + length[1] * (x2 + length[2] * x3));
  default: return -1;
  }
}

inline int
wrap_coord(const int x, const int L)
{
  assert((-L <= x) && (x < 2 * L));
  return x + L * ((x < 0) - (x >= L));
}

template <std::size_t D, std::size_t N = D>
inline int
flatten_wrap_coords(const std::array<int,N> length,
                    const int x0,
                    const int x1 = 0,
                    const int x2 = 0,
                    const int x3 = 0)
{
  static_assert(D <= 4, "D > 4 not supported yet!");
  assert(D <= N);

  switch(D) {
  case 0 : return 0;
  case 1 : return flatten_coords<1>(length, wrap_coord(x0, length[0]));
  case 2 : return flatten_coords<2>(length, wrap_coord(x0, length[0]),
                                    wrap_coord(x1, length[1]));
  case 3 : return flatten_coords<3>(length, wrap_coord(x0, length[0]),
                                    wrap_coord(x1, length[1]),
                                    wrap_coord(x2, length[2]));
  case 4 : return flatten_coords<4>(length, wrap_coord(x0, length[0]),
                                    wrap_coord(x1, length[1]),
                                    wrap_coord(x2, length[2]),
                                    wrap_coord(x3, length[3]));
  default: return -1;
  }
}

template <std::size_t D, int dx0, int dx1 = 0, int dx2 = 0, int dx3 = 0, std::size_t N = D>
inline bool
disp_crosses_face(const std::array<int,N> length,
                  const int x0,
                  const int x1 = 0,
                  const int x2 = 0,
                  const int x3 = 0)
{
  static_assert(D <=  4, "D > 4 not supported yet!");
  static_assert(((dx0 == 0) || (dx0 == -1) || (dx0 == +1) || (dx0 == 'b')), "invalid displacement!");
  static_assert(((dx1 == 0) || (dx1 == -1) || (dx1 == +1) || (dx1 == 'b')), "invalid displacement!");
  static_assert(((dx2 == 0) || (dx2 == -1) || (dx2 == +1) || (dx2 == 'b')), "invalid displacement!");
  static_assert(((dx3 == 0) || (dx3 == -1) || (dx3 == +1) || (dx3 == 'b')), "invalid displacement!");
  assert(D <= N);

  if (D > 0) if (dx0 == -1) if (!(x0 > 0              )) return true;
  if (D > 0) if (dx0 =='b') if (!(x0 > 0              )) return true;
  if (D > 0) if (dx0 == +1) if (!(x0 < (length[0] - 1))) return true;
  if (D > 0) if (dx0 =='b') if (!(x0 < (length[0] - 1))) return true;
  if (D > 1) if (dx1 == -1) if (!(x1 > 0              )) return true;
  if (D > 1) if (dx1 =='b') if (!(x1 > 0              )) return true;
  if (D > 1) if (dx1 == +1) if (!(x1 < (length[1] - 1))) return true;
  if (D > 1) if (dx1 =='b') if (!(x1 < (length[1] - 1))) return true;
  if (D > 2) if (dx2 == -1) if (!(x2 > 0              )) return true;
  if (D > 2) if (dx2 =='b') if (!(x2 > 0              )) return true;
  if (D > 2) if (dx2 == +1) if (!(x2 < (length[2] - 1))) return true;
  if (D > 2) if (dx2 =='b') if (!(x2 < (length[2] - 1))) return true;
  if (D > 3) if (dx3 == -1) if (!(x3 > 0              )) return true;
  if (D > 3) if (dx3 =='b') if (!(x3 > 0              )) return true;
  if (D > 3) if (dx3 == +1) if (!(x3 < (length[3] - 1))) return true;
  if (D > 3) if (dx3 =='b') if (!(x3 < (length[3] - 1))) return true;
  return false;
}

} // namespace qtools

#endif // QTOOLS_MULTIDIM_H
