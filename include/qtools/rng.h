#ifndef QTOOLS_RNG_H
#define QTOOLS_RNG_H

#include <stdlib.h>

#ifdef QTOOLS_USE_RNG_GSL
#include <gsl/gsl_rng.h>

namespace qtools {

typedef gsl_rng rng_type;

class rng_vector
{
  public:
  typedef rng_type* pointer;

  rng_vector(const int n,
             const unsigned seed1 = 0,
             const unsigned seed2 = 0)
  : size_(n)
  {
    elems_ = (pointer*)malloc(sizeof(pointer) * n);
    gsl_rng_env_setup();
    const gsl_rng_type* T = gsl_rng_default;
    for (int i = 0; i < n; i++) {
      elems_[i] = gsl_rng_alloc(T);
      gsl_rng_set(elems_[i], 123 * (i + 1) + 456 * seed1 + seed2);
    }
  }

  ~rng_vector()
  {
    for (int i = 0; i < size(); i++) {
      gsl_rng_free(elems_[i]);
    } 
  }

  inline const pointer&
  operator[](const int i) const
  { return elems_[i]; }

  inline pointer&
  operator[](const int i)
  { return elems_[i]; }

  int size() const
  { return size_; }

  private:
  const int size_;
  pointer* elems_;
};

inline double
rng_uniform(rng_type* rng)
{ return gsl_rng_uniform(rng); }

} // namespace qtools

#else

namespace qtools {

typedef unsigned int rng_type;

class rng_vector
{
  public:
  typedef rng_type* pointer;

  rng_vector(const int n,
             const unsigned seed1 = 0,
             const unsigned seed2 = 0)
  : size_(n)
  {
    elems_ = (pointer*)malloc(sizeof(pointer) * n);
    for (int i = 0; i < n; i++) {
      elems_[i] = (pointer)malloc(sizeof(rng_type));
      elems_[i][0] = 123 * (i + 1) + 456 * seed1 + seed2;
    }
  }

  ~rng_vector()
  {
    for (int i = 0; i < size(); i++) {
      free(elems_[i]);
    }
    free(elems_);
  }

  inline const pointer&
  operator[](const int i) const
  { return elems_[i]; }

  inline pointer&
  operator[](const int i)
  { return elems_[i]; }

  int size() const
  { return size_; }

  private:
  const int size_;
  pointer* elems_;
};

inline double
rng_uniform(rng_type* rng)
{ 
  *rng = 1664525 * (*rng) + 1013904223;
  return *rng * 2.32830643708079e-10;
}

} // namespace qtools

#endif

#endif // QTOOLS_RNG_H
