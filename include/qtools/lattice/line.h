#ifndef QTOOLS_LATTICE_LINE_H
#define QTOOLS_LATTICE_LINE_H

#include <array>
#include <cassert>
#include <qtools/lattice/name.h>

namespace qtools {
namespace lattice {

template <>
class info<name::line>
{
  public:
  static constexpr int dimension                          = 1;
  static constexpr int max_num_outgoing_edges_per_vertex  = 1;

  template <std::size_t N>
  static int
  num_vertices(const std::array<int,N> length)
  {
    assert((int)N >= dimension);
    return length[0];
  }

  template <std::size_t N>
  static int
  num_edges(const std::array<int,N> length)
  {
    assert((int)N >= dimension);
    return length[0];
  }

  template <std::size_t N>
  static int
  max_num_outgoing_edges(const std::array<int,N> length)
  {
    assert((int)N >= dimension);
    return length[0];
  }

  template <std::size_t N, typename StuffFn>
  static void
  for_each_vertex(const std::array<int,N> length,
                  const StuffFn& stuff)
  {
    assert((int)N >= dimension);
    for (int x = 0; x < length[0]; x++) {
      stuff(x);
    }
    return;
  }

  template <std::size_t N, typename StuffFn>
  static void
  for_each_sublattice_vertex(const std::array<int,N> length,
                             const StuffFn& stuff)
  {
    assert((int)N >= dimension);
    for (int x = 0; x < length[0]; x+=2) {
      stuff(x);
    }
    for (int x = 1; x < length[0]; x+=2) {
      stuff(x);
    }
    return;
  }   
};

} // namespace lattice
} // namespace qtools

#endif // QTOOLS_LATTICE_LINE_H
