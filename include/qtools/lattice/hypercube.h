#ifndef QTOOLS_LATTICE_HYPERCUBE_H
#define QTOOLS_LATTICE_HYPERCUBE_H

#include <array>
#include <cassert>
#include <qtools/lattice/name.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace qtools {
namespace lattice {

template <>
class info<name::hypercube>
{
  public:
  static constexpr int dimension                          = 4;
  static constexpr int max_num_outgoing_edges_per_vertex  = 4;

  template <std::size_t N>
  static int
  num_vertices(const std::array<int,N> length)
  {
    assert((int)N >= dimension);
    return length[0] * length[1] * length[2] * length[3];
  }

  template <std::size_t N>
  static int
  num_edges(const std::array<int,N> length)
  {
    assert((int)N >= dimension);
    return 4 * length[0] * length[1] * length[2] * length[3];
  }

  template <std::size_t N>
  static int
  max_num_outgoing_edges(const std::array<int,N> length)
  {
    assert((int)N >= dimension);
    return 4 * length[0] * length[1] * length[2] * length[3];
  }

  template <std::size_t N, typename StuffFn>
  static void
  for_each_vertex(const std::array<int,N> length,
                  const StuffFn& stuff)
  {
    assert((int)N >= dimension);
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 0; t < length[3]; t++) {
    for (int z = 0; z < length[2]; z++) {
    for (int y = 0; y < length[1]; y++) {
    for (int x = 0; x < length[0]; x++) {
      stuff(x,y,z,t);
    }}}}
    return;
  }

  template <std::size_t N, typename StuffFn>
  static void
  for_each_sublattice_vertex(const std::array<int,N> length,
                             const StuffFn& stuff)
  {
    assert((int)N >= dimension);
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 0; t < length[3]; t+=2) {
    for (int z = 0; z < length[2]; z+=2) {
    for (int y = 0; y < length[1]; y+=2) {
    for (int x = 0; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 0; t < length[3]; t+=2) {
    for (int z = 0; z < length[2]; z+=2) {
    for (int y = 0; y < length[1]; y+=2) {
    for (int x = 1; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 0; t < length[3]; t+=2) {
    for (int z = 0; z < length[2]; z+=2) {
    for (int y = 1; y < length[1]; y+=2) {
    for (int x = 0; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 0; t < length[3]; t+=2) {
    for (int z = 0; z < length[2]; z+=2) {
    for (int y = 1; y < length[1]; y+=2) {
    for (int x = 1; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 0; t < length[3]; t+=2) {
    for (int z = 1; z < length[2]; z+=2) {
    for (int y = 0; y < length[1]; y+=2) {
    for (int x = 0; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 0; t < length[3]; t+=2) {
    for (int z = 1; z < length[2]; z+=2) {
    for (int y = 0; y < length[1]; y+=2) {
    for (int x = 1; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 0; t < length[3]; t+=2) {
    for (int z = 1; z < length[2]; z+=2) {
    for (int y = 1; y < length[1]; y+=2) {
    for (int x = 0; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 0; t < length[3]; t+=2) {
    for (int z = 1; z < length[2]; z+=2) {
    for (int y = 1; y < length[1]; y+=2) {
    for (int x = 1; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 1; t < length[3]; t+=2) {
    for (int z = 0; z < length[2]; z+=2) {
    for (int y = 0; y < length[1]; y+=2) {
    for (int x = 0; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 1; t < length[3]; t+=2) {
    for (int z = 0; z < length[2]; z+=2) {
    for (int y = 0; y < length[1]; y+=2) {
    for (int x = 1; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 1; t < length[3]; t+=2) {
    for (int z = 0; z < length[2]; z+=2) {
    for (int y = 1; y < length[1]; y+=2) {
    for (int x = 0; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 1; t < length[3]; t+=2) {
    for (int z = 0; z < length[2]; z+=2) {
    for (int y = 1; y < length[1]; y+=2) {
    for (int x = 1; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 1; t < length[3]; t+=2) {
    for (int z = 1; z < length[2]; z+=2) {
    for (int y = 0; y < length[1]; y+=2) {
    for (int x = 0; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 1; t < length[3]; t+=2) {
    for (int z = 1; z < length[2]; z+=2) {
    for (int y = 0; y < length[1]; y+=2) {
    for (int x = 1; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 1; t < length[3]; t+=2) {
    for (int z = 1; z < length[2]; z+=2) {
    for (int y = 1; y < length[1]; y+=2) {
    for (int x = 0; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int t = 1; t < length[3]; t+=2) {
    for (int z = 1; z < length[2]; z+=2) {
    for (int y = 1; y < length[1]; y+=2) {
    for (int x = 1; x < length[0]; x+=2) {
      stuff(x,y,z,t);
    }}}}
    return;
  }   
};

} // namespace lattice
} // namespace qtools

#endif // QTOOLS_LATTICE_HYPERCUBE_H
