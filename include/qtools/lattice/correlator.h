#ifndef QTOOLS_LATTICE_CORRELATOR_H
#define QTOOLS_LATTICE_CORRELATOR_H

#include <array>
#include <vector>
#include <cassert>
#include <algorithm>
#include <iostream>
#include <qtools/multidim.h>
#include <qtools/lattice/list.h>

namespace qtools {
namespace lattice {

template <name LatNm, typename T = double>
class correlator
{
  public:
  static constexpr const int D = info<LatNm>::dimension;

  correlator(const std::array<int,D> init_length)
  : points_per_config_(info<LatNm>::num_vertices(init_length)),
    length_(init_length)
  {
    static_assert(D <= 4, "D > 4 not supported yet!");
    corr_.resize(*std::min_element(init_length.begin(),init_length.end())/2);
    std::fill(corr_.begin(),corr_.end(),0);
  }

  double distance(const int i) const
  { return i * std::sqrt(D); }

  int size() const
  { return corr_.size(); }

  T operator()(const int i) const
  {
    assert((0 <= i) && (i < size()));
    return corr_[i] / (1. * config_count_ * points_per_config_); 
  }

  int count() const
  { return config_count_; }

  void clear()
  {
    config_count_ = 0;
    std::fill(corr_.begin(),corr_.end(),0);
    return;
  }

  friend std::ostream&
  operator<<(std::ostream& output, const correlator<LatNm>& C)
  {
    for (int i = 0; i < C.size(); i++) {
      output << C.distance(i) << "\t" << C(i) << std::endl;
    }
    return output;
  }

  template <typename TVar, typename CorrFn>
  void add_data(const TVar* config, const CorrFn& corrfunc)
  {
    switch(LatNm) {
    case name::line:
      info<name::line>::for_each_vertex(length_,
        [&](const int x) {
          const auto value0 = config[flatten_coords<D>(length_,x)];
          for (int i = 0; i < size(); i++) {
            const auto valuer = config[flatten_wrap_coords<D>(length_,x+i)];
            corr_[i] += corrfunc(value0,valuer);
          }
        }
      );
    break;
    case name::square:
      info<name::square>::for_each_vertex(length_,
        [&](const int x, const int y) {
          const auto value0 = config[flatten_coords<D>(length_,x,y)];
          for (int i = 0; i < size(); i++) {
            const auto valuer = config[flatten_wrap_coords<D>(length_,x+i,y+i)];
            corr_[i] += corrfunc(value0,valuer);
          }
        }
      );
    break;
    case name::cube:
      info<name::cube>::for_each_vertex(length_,
        [&](const int x, const int y, const int z) {
          const auto value0 = config[flatten_coords<D>(length_,x,y,z)];
          for (int i = 0; i < size(); i++) {
            const auto valuer = config[flatten_wrap_coords<D>(length_,x+i,y+i,z+i)];
            corr_[i] += corrfunc(value0,valuer);
          }
        }
      );
    break;
    case name::hypercube:
      info<name::hypercube>::for_each_vertex(length_,
        [&](const int x, const int y, const int z, const int t) {
          const auto value0 = config[flatten_coords<D>(length_,x,y,z)];
          for (int i = 0; i < size(); i++) {
            const auto valuer = config[flatten_wrap_coords<D>(length_,x+i,y+i,z+i,t+i)];
            corr_[i] += corrfunc(value0,valuer);
          }
        }
      );
    break;
    }
    config_count_++;
    return;
  }

  private:
  int                     config_count_       = 0;
  int                     points_per_config_  = 0;
  const std::array<int,D> length_             {{0}};
  std::vector<T>          corr_               {{0}};
};

} // namespace lattice
} // namespace qtools

#endif // QTOOLS_LATTICE_CORRELATOR_H
