#ifndef QTOOLS_LATTICE_LIST_H
#define QTOOLS_LATTICE_LIST_H

#include <qtools/lattice/line.h>
#include <qtools/lattice/square.h>
#include <qtools/lattice/cube.h>
#include <qtools/lattice/hypercube.h>

#endif // QTOOLS_LATTICE_LIST_H
