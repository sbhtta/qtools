#ifndef QTOOLS_LATTICE_SQUARE_H
#define QTOOLS_LATTICE_SQUARE_H

#include <array>
#include <cassert>
#include <qtools/lattice/name.h>

#ifdef _OPENMP
#include <omp.h>
#endif

namespace qtools {
namespace lattice {

template <>
class info<name::square>
{
  public:
  static constexpr int dimension                          = 2;
  static constexpr int max_num_outgoing_edges_per_vertex  = 2;

  template <std::size_t N>
  static int
  num_vertices(const std::array<int,N> length)
  {
    assert((int)N >= dimension);
    return length[0] * length[1];
  }

  template <std::size_t N>
  static int
  num_edges(const std::array<int,N> length)
  {
    assert((int)N >= dimension);
    return 2 * length[0] * length[1];
  }

  template <std::size_t N>
  static int
  max_num_outgoing_edges(const std::array<int,N> length)
  {
    assert((int)N >= dimension);
    return 2 * length[0] * length[1];
  }

  template <std::size_t N, typename StuffFn>
  static void
  for_each_vertex(const std::array<int,N> length,
                  const StuffFn& stuff)
  {
    assert((int)N >= dimension);
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int y = 0; y < length[1]; y++) {
    for (int x = 0; x < length[0]; x++) {
      stuff(x,y);
    }}
    return;
  }

  template <std::size_t N, typename StuffFn>
  static void
  for_each_sublattice_vertex(const std::array<int,N> length,
                             const StuffFn& stuff)
  {
    assert((int)N >= dimension);
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int y = 0; y < length[1]; y+=2) {
    for (int x = 0; x < length[0]; x+=2) {
      stuff(x,y);
    }}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int y = 0; y < length[1]; y+=2) {
    for (int x = 1; x < length[0]; x+=2) {
      stuff(x,y);
    }}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int y = 1; y < length[1]; y+=2) {
    for (int x = 0; x < length[0]; x+=2) {
      stuff(x,y);
    }}
    #ifdef _OPENMP
    #pragma omp for
    #endif
    for (int y = 1; y < length[1]; y+=2) {
    for (int x = 1; x < length[0]; x+=2) {
      stuff(x,y);
    }}
    return;
  }   
};

} // namespace lattice
} // namespace qtools

#endif // QTOOLS_LATTICE_SQUARE_H
