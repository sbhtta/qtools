#ifndef QTOOLS_LATTICE_NAME_H
#define QTOOLS_LATTICE_NAME_H

namespace qtools {
namespace lattice {

enum class name {
  line,
	square,
	cube,
  hypercube,
};

template <name LatNm>
class info;

} // namespace lattice
} // namespace qtools

#endif // QTOOLS_LATTICE_NAME_H
