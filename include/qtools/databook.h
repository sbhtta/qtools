#ifndef QTOOLS_DATABOOK_H
#define QTOOLS_DATABOOK_H

#include <vector>
#include <iostream>
#include <algorithm>
#include <qtools/autocorrelator.h>

namespace qtools {

enum class databook_defaultenum { none, all };

template<typename T, std::size_t N, typename EnumT = databook_defaultenum>
class databook
{
	public:
	databook(const int init_num_points,
           const int init_num_blocks = 1)
  : num_points_(init_num_points),
    num_blocks_(init_num_blocks),
    num_points_per_block_(init_num_points / init_num_blocks)
  {
    data_.resize(num_vars());
    for (int ivar = 0; ivar < num_vars(); ivar++) {
      data_[ivar].resize(num_blocks());
      for (int block = 0; block < num_blocks(); block++) {
        data_[ivar][block].resize(num_points_per_block());
      }
    }
    autocorrelator_.resize(num_vars());
    active_block_.resize(num_vars());
    active_point_.resize(num_vars());
    clear();
	}

  int
  num_vars() const
  { return N; }

  int
  num_points() const
  { return num_points_; }

  int
  num_blocks() const
  { return num_blocks_; }

  int
  num_points_per_block() const
  { return num_points_per_block_; }

  int
  maxlag() const
  { return maxlag_; }

  void
  set_maxlag(const int init_maxlag)
  {
    maxlag_ = init_maxlag;
    for (int ivar = 0; ivar < num_vars(); ivar++) {
      autocorrelator_[ivar].set_maxlag(maxlag());
    }
    return;
  }

  void
  clear()
  {
    for (int ivar = 0; ivar < num_vars(); ivar++) {
      for (int block = 0; block < num_blocks(); block++) {
        for (int t = 0; t < num_points_per_block(); t++) {
          data_[ivar][block][t] = 0;
        }
      }
    }
    for (int ivar = 0; ivar < num_vars(); ivar++) {
      autocorrelator_[ivar].clear();
      active_point_[ivar] = 0;
      active_block_[ivar] = 0;
    }
    return;
  }

	autocorrelator<T>&
	autocorrelation(const int ivar = 0)
	{
    assert(ivar < num_vars());
    return autocorrelator_[ivar];
  }
	const autocorrelator<T>&
	autocorrelation(const int ivar = 0) const
	{
    assert(ivar < num_vars());
    return autocorrelator_[ivar];
  }
	autocorrelator<T>&
  autocorrelation(const EnumT var)
	{ 
    assert(static_cast<int>(var) < num_vars());
    return autocorrelator_[static_cast<int>(var)];
  }
	const autocorrelator<T>&
  autocorrelation(const EnumT var) const
	{
    assert(static_cast<int>(var) < num_vars());
    return autocorrelator_[static_cast<int>(var)];
  }

	std::vector<T>&
  data(const int ivar = 0, const int block = 0)
	{ 
    assert(ivar < num_vars());
    assert(block < num_blocks());
    return data_[ivar][block];
  }
	const std::vector<T>&
  data(const int ivar = 0, const int block = 0) const
	{
    assert(ivar < num_vars());
    assert(block < num_blocks());
    return data_[ivar][block];
  }
	std::vector<T>&
  data(const EnumT var, const int block = 0)
	{ 
    assert(static_cast<int>(var) < num_vars());
    assert(block < num_blocks());
    return data_[static_cast<int>(var)][block];
  }
	const std::vector<T>&
  data(const EnumT var, const int block = 0) const
	{
    assert(static_cast<int>(var) < num_vars());
    assert(block < num_blocks());
    return data_[static_cast<int>(var)][block];
  }

	void
  add_data(const int ivar, const T value)
	{
    assert(ivar < num_vars());
    assert(active_block_[ivar] < num_blocks());
    assert(active_point_[ivar] < num_points_per_block());
    data_[ivar][active_block_[ivar]][active_point_[ivar]] = value;
    if (maxlag() > 0) { autocorrelator_[ivar].add_data(value); }
    if (active_point_[ivar] >= (num_points_per_block() - 1)) {
      active_point_[ivar]  = 0;
      active_block_[ivar] += 1;
    } else {
      active_point_[ivar] += 1;
    }
    return;
  }
	void
  add_data(const EnumT var, const T value)
	{
    const auto ivar = static_cast<int>(var);
    add_data(ivar, value);
    return;
  }
	void
  add_data(const T value)
	{
    add_data(0, value);
    return;
  }

  friend std::ostream&
  operator<<(std::ostream& output, const databook<T,N,EnumT>& db)
  {
    for (int block = 0; block < db.num_blocks(); block++) {
      for (int t = 0; t < db.num_points_per_block(); t++) {
        for (int ivar = 0; ivar < db.num_vars(); ivar++) {
          output << std::scientific << db.data_[ivar][block][t] << "\t";
        }
        output << std::endl;
      }
    }
    return output;
  }

  friend std::istream&
  operator>>(std::istream& input, databook<T,N,EnumT>& db)
  {
    int ivar = 0, t = 0;
    double tmp;
    while ((input >> tmp) && (t < db.num_points())) {
      db.add_data(ivar,tmp);
      ivar++;
      if (ivar == (int)db.num_vars()) {
        ivar = 0;
        t++;
      }
    }
    return input;
  }

	int                                               maxlag_       = 0;
  const int                                         num_vars_     = 1;
  const int                                         num_points_   = 0;
  const int                                         num_blocks_   = 1;
  const int                                         num_points_per_block_ = 0;
  std::vector<int>                                  active_point_;
  std::vector<int>                                  active_block_;
	std::vector< autocorrelator<T> >                  autocorrelator_;
	std::vector< std::vector< std::vector<T> > >      data_;
};

} // namespace qtools

#endif // QTOOLS_DATABOOK_H
