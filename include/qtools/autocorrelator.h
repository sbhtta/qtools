#ifndef QTOOLS_AUTOCORRELATOR_H
#define QTOOLS_AUTOCORRELATOR_H

#include <vector>
#include <deque>
#include <cmath>
#include <cassert>
#include <iostream>
#include <fstream>

namespace qtools {

template <typename T>
class autocorrelator
{
	public:
	autocorrelator(const int init_maxlag = 0)
	{ set_maxlag(init_maxlag); }

  int maxlag() const
	{ return sum_xilag_.size() - 2; }

  void set_maxlag(const int init_maxlag)
  {
	  sum_xilag_.resize(init_maxlag + 2);
	  sum_xi_xilag_.resize(init_maxlag + 2);
		clear();
    return;
  }
  
  int size() const
  { return maxlag() + 1; }

  void resize(const int init_size)
  {
    set_maxlag(init_size - 1);
    return;
  }

	void clear()
	{
		accumulate_count_ = 0;
		mean_ = T();
    sq_mean_ = 0.;
    sum_xi_ = T();
    std::fill(sum_xi_xilag_.begin(), sum_xi_xilag_.end(), 0.);
    std::fill(sum_xilag_.begin(), sum_xilag_.end(), T());
		last_maxlag_data_.resize(0);
		return;
	}

  double operator()(const int lag) const
	{
    if (lag > maxlag()) {
      return 0.;
    } else {
      T tmp_sum_xi_ = sum_xi_;
      for (int i = 0; i <= (maxlag() - lag); i++) {
        tmp_sum_xi_ += last_maxlag_data_[i];
      }
      const double numer = sum_xi_xilag_[lag]
                         - (tmp_sum_xi_ + sum_xilag_[lag]) * mean_
                         + (accumulate_count_ - lag) * (mean_ * mean_);
      const double denom = accumulate_count_ * (sq_mean_ - mean_ * mean_);
      return numer / denom;
    }
	}

	double 
	tau(const int uncorrelated_count)
	{
		const double optimal_cutoff
      = (0.5 * std::log(0.5 * uncorrelated_count) - 1.);
		double sum = operator()(0) + operator()(1);
		for (int i = 2; i < maxlag(); i++) {
			sum += operator()(i);
			if (i > (optimal_cutoff * (1. + 2. * sum))) break;
		}
		return (1. + 2. * sum);
	}

	void add_data(const T data)
	{
		last_maxlag_data_.push_back(data);
    for (int lag = 0; lag < (int)(last_maxlag_data_.size()); lag++) {
      sum_xilag_[lag] += data;
		  sum_xi_xilag_[lag]
        += data * last_maxlag_data_[(int)last_maxlag_data_.size() - 1 - lag];
    }
		if ((int)last_maxlag_data_.size() > (maxlag() + 1)) {
      sum_xi_ += last_maxlag_data_[0];
			last_maxlag_data_.pop_front();
		}
		mean_     += (data - mean_) / (accumulate_count_ + 1.);
    sq_mean_  += (data * data - sq_mean_) / (accumulate_count_ + 1.);
		accumulate_count_++;
		return;
	}

  friend std::ostream&
  operator<<(std::ostream& output, const autocorrelator A)
  {
    for (int i = 0; i <= A.maxlag(); i++) {
      output << std::scientific << i << "\t" << A(i) << std::endl;
    }
    return output;
  }

	private:
	int                 accumulate_count_;
	T                   mean_;
	double              sq_mean_;
  T                   sum_xi_;
	std::vector<T>      sum_xilag_;
	std::vector<double> sum_xi_xilag_;
	std::deque<T>       last_maxlag_data_;
};

} // namespace qtools

#endif // QTOOLS_AUTOCORRELATOR_H
