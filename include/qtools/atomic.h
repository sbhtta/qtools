#ifndef QTOOLS_ATOMIC_H
#define QTOOLS_ATOMIC_H

namespace qtools {

template <typename T>
inline void
atomic_add(volatile T* ptr, const T n) 
{
  for (;;) {
    T current_value = *ptr;
    T new_value = *ptr + n;
    if (__sync_bool_compare_and_swap(ptr, current_value, new_value)) {
      return;
    }
  }
}

template <typename T>
inline T 
atomic_set_min(volatile T* ptr, const T n) 
{
  for (;;) {
    T current_value = *ptr;
    if (current_value <= n) {
      return current_value;
    }
    if (__sync_bool_compare_and_swap(ptr, current_value, n)) {
      return current_value;
    }
  }
}

} // namespace qtools

#endif // QTOOLS_ATOMIC_H
