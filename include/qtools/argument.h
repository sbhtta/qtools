#ifndef QTOOLS_ARGUMENT_H
#define QTOOLS_ARGUMENT_H

#include <string>

namespace qtools {

bool
find_argument(const int         argc,
              char**            argv,
              const std::string argstring)
{
  for (int i = 0; i < argc; i++) {
    const std::string tmp = argv[i];
    if (tmp == argstring) {
      return true;
    }
  }
  return false;
}

int
locate_argument(const int         argc,
                char**            argv,
                const std::string argstring)
{
  for (int i = 0; i < argc; i++) {
    const std::string tmp = argv[i];
    if (tmp == argstring) {
      return i;
    }
  }
  return -1;
}

double
fetch_argument_double(const int         argc,
                      char**            argv,
                      const std::string argstring,
                      const double      default_value)
{
  if (find_argument(argc, argv, argstring)) {
    return std::stod(argv[locate_argument(argc, argv, argstring)+1]);
  }
  return default_value;
}

double
fetch_argument_double(const int    argc,
                      char**       argv,
                      const int    i,
                      const double default_value)
{
  if (i < argc) { 
    return std::stod(argv[i]);
  } else {
    return default_value;
  }
}

int
fetch_argument_int(const int         argc,
                   char**            argv,
                   const std::string argstring,
                   const int         default_value)
{ 
  if (find_argument(argc, argv, argstring)) {
    return std::stoi(argv[locate_argument(argc, argv, argstring)+1]);
  }
  return default_value;
}

int
fetch_argument_int(const int argc,
                   char**    argv,
                   const int i,
                   const int default_value)
{
  if (i < argc) { 
    return std::stoi(argv[i]);
  } else {
    return default_value;
  }
}

std::string
fetch_argument_string(const int         argc,
                      char**            argv,
                      const std::string argstring,
                      const std::string default_value)
{ 
  if (find_argument(argc, argv, argstring)) {
    const std::string tmp2 = argv[locate_argument(argc, argv, argstring)+1];
    return tmp2;
  }
  return default_value;
}

std::string
fetch_argument_string(const int         argc,
                      char**            argv,
                      const int         i,
                      const std::string default_value)
{
  if (i < argc) { 
    const std::string tmp2 = argv[i];
    return tmp2;
  } else {
    return default_value;
  }
}

} // namespace qtools

#endif // QTOOLS_ARGUMENT_H
