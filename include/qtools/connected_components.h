#ifndef QTOOLS_CONNECTED_COMPONENTS_H
#define QTOOLS_CONNECTED_COMPONENTS_H

#include <vector>
#include <cassert>
#include <qtools/atomic.h>

namespace qtools {

class connected_components
{
  public:
  int size() const
  { return num_labels_; }

  int capacity() const
  { return label_.size(); }

  void resize(const int n)
  {
    if ((int)label_.size() < n) {
      label_.resize(n);
    }
    num_labels_ = n;
    return;
  }

  void clear()
  {
    label_updated_              = false;
    label_histogram_updated_    = false;
    largest_component_updated_  = false;
    largest_component_size_     = -1;
    largest_component_label_    = -1;
    return;
  }

  void set()
  {
    label_updated_ = true;
    return;
  }

  bool is_label_updated() const
  { return label_updated_; }

  int label(const int i) const
  {
    assert((0 <= i) && (i < size()));
    return label_[i];
  }

  int& label(const int i)
  {
    assert((0 <= i) && (i < size()));
    return label_[i];
  }

  int root(const int i) const
  {
    assert((0 <= i) && (i < size()));
    int j = i;
    while (j != label_[j]) { j = label_[j]; }
    return j;
  }

  void merge_roots(const int i, const int j)
  {
    assert(!is_label_updated());
    assert((0 <= i) && (i < size()));
    assert((0 <= j) && (j < size()));
    const int rooti = root(i);
    const int rootj = root(j);
    if (rootj < rooti) { label_[rooti] = rootj; }
    else               { label_[rootj] = rooti; }
    return;
  }

  void atomic_merge_roots(const int i, const int j)
  {
    assert(!is_label_updated());
    int rooti = root(i);
    int rootj = root(j);
    for (;;) {
      const auto tmp = qtools::atomic_set_min(&label_[rooti], rootj);
      if (tmp == rootj)      {break;}
      else if (tmp > rootj)  {rooti = tmp;}
      else if (tmp < rootj)  {rooti = rootj; rootj = tmp;}
    }
    return;
  }

  void initialize()
  {
    clear();
    for (int i = 0; i < size(); i++) {
      label_[i] = i;
    }
    return;
  }

  void finalize()
  {
    for (int i = 0; i < size(); i++) {
      label_[i] = root(label_[i]);
    }
    set();
    return;
  }

  int component_size_of_label(const int i)
  {
    if (!is_label_histogram_updated()) {
      update_label_histogram();
    }
    assert((0 <= i) && (i < size()));
    return label_histogram_[i];
  }

  int nocheck_component_size_of_label(const int i) const
  {
    assert((0 <= i) && (i < size()));
    return label_histogram_[i];
  }

	int largest_component_size()
	{	
		if (!is_largest_component_updated())
    { update_largest_component(); }
		return largest_component_size_; 
	}

  int nocheck_largest_component_size() const
  { return largest_component_size_; }

	int largest_component_label()
	{ 
		if (!is_largest_component_updated())
    { update_largest_component(); }
		return largest_component_label_; 
	}

  int nocheck_largest_component_label() const
  { return largest_component_label_; }

  void bin_all_component_sizes_to_distribution(std::vector<long long int>& dist)
  {
    if ((int)dist.size() < (size() + 1)) {
      dist.resize(size() + 1);
      for (int i = 0; i < (int)dist.size(); i++) {
        dist[i] = 0;
      }
    }
    if (!is_label_histogram_updated()) {
      update_label_histogram();
    }
    for (int i = 0; i < size(); i++) {
      const auto size_i = label_histogram_[i];
      // as no discrimination between valid and invalid
      // elements are made during the labelling stage,
      // invalid elements form components of size one on
      // their own, so we will bin only those components
      // which have two or more elements: 
      if (size_i > 1) {
        dist[size_i] += 1;
      }
    }
  }

  bool is_label_histogram_updated() const
  { return label_histogram_updated_; }
   
	void update_label_histogram()
	{
    assert(is_label_updated());
		if ((int)label_histogram_.size() < size()) {
    	label_histogram_.resize(size());
		}
    for (int i = 0; i < size(); i++) {
      label_histogram_[i] = 0;
    }
    for (int i = 0; i < size(); i++) {
      label_histogram_[label(i)] += 1;
    }
		label_histogram_updated_ = true;
		return;
	}

  bool is_largest_component_updated() const
  { return largest_component_updated_; }
   
	void update_largest_component()
	{
		if (!is_label_histogram_updated()) {
			update_label_histogram();
		}
		largest_component_size_ = 0;
		largest_component_label_ = 0;
    for (int i = 0; i < size(); i++) {
      const int size_i = label_histogram_[i];
      if (size_i > largest_component_size_) {
        largest_component_size_  = size_i;
				largest_component_label_ = i;
      }
    }
		largest_component_updated_ = true;
		return;
	}

  private:
  bool  label_updated_              = false;
  bool  label_histogram_updated_    = false;
  bool  largest_component_updated_  = false;

  int   num_labels_                 = -1;
  int   largest_component_size_     = -1;
  int   largest_component_label_    = -1;

  std::vector<int> label_          {{0}};
  std::vector<int> label_histogram_{{0}};
};

} // namespace qtools

#endif // QTOOLS_CONNECTED_COMPONENTS_H
