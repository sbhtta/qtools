#ifndef QTOOLS_STATISTICS_H
#define QTOOLS_STATISTICS_H

#include <array>
#include <vector>
#include <deque>
#include <cmath>
#include <cassert>
#include <vector>
#include <numeric>
#include <iostream>

namespace qtools {

double average_component_size(const std::vector<long long int>& dist)
{
  long double num = 0., dem = 0.;
  int count = 0;
  for (int s = 0; s < (int)dist.size(); s++) {
    const auto ns = dist[s];
    const long double tmp = (s * ns) / (1. * dist.size());
    num += (s * tmp - num) / (long double)(count + 1.);
    dem += (    tmp - dem) / (long double)(count + 1.);
    count++;
    assert(ns >= 0);
    assert(tmp >= 0);
  }
  assert(num >= 0);
  assert(dem >= 0);
  return num / dem;
}

template<std::size_t R, class C>
double
sample_moment(const C& sample, const double mean_value)
{
  typedef typename C::value_type T;
  return std::accumulate(sample.begin(), sample.end(), T(),
    [&](const T cumulative, const T value)
    { return cumulative + std::pow((value - mean_value), R); })
    / (1. * sample.size());
}

template<std::size_t R, class C>
double
moment(const C& sample, const double mean_value)
{ return sample_moment(sample, mean_value); }

template<std::size_t R, class C>
double
sample_abs_moment(const C& sample, const double mean_value)
{
  typedef typename C::value_type T;
  return std::accumulate(sample.begin(), sample.end(), T(),
    [&](const T cumulative, const T value)
    { return cumulative + std::pow(std::abs(value - mean_value), R); })
    / (1. * sample.size());
}

template<std::size_t R, class C>
double
abs_moment(const C& sample, const double mean_value)
{ return sample_abs_moment(sample, mean_value); }

template<std::size_t R, class C>
double
sample_raw_moment(const C& sample)
{ return sample_moment<R>(sample, 0.); }

template<std::size_t R, class C>
double
raw_moment(const C& sample)
{ return sample_raw_moment<R>(sample); }

template<std::size_t R, class C>
double
sample_central_moment(const C& sample)
{ return sample_moment<R>(sample, sample_raw_moment<1>(sample)); }

template<std::size_t R, class C>
double
central_moment(const C& sample)
{ return sample_central_moment<R>(sample); }

template<class C>
double
sample_mean(const C& sample)
{ return sample_raw_moment<1>(sample); }

template<class C>
double
unbiased_sample_mean(const C& sample)
{ return sample_mean(sample); }

template<class C>
double
mean(const C& sample)
{ return unbiased_sample_mean(sample); }

template<class C>
double
sample_variance(const C& sample)
{ return sample_central_moment<2>(sample); }

template<class C>
double
unbiased_sample_variance(const C& sample)
{ return sample_variance(sample) * sample.size() / (sample.size() - 1.0); }

template<class C>
double
variance(const C& sample)
{ return unbiased_sample_variance(sample); }

template<class C>
double
sample_standard_deviation(const C& sample)
{ return std::sqrt(sample_variance(sample)); }
  
template<class C>
double
unbiased_sample_standard_deviation(const C& sample)
{ return std::sqrt(unbiased_sample_variance(sample)); }

template<class C>
double
standard_deviation(const C& sample)
{ return unbiased_sample_standard_deviation(sample); }

template<class C>
double
skewness(const C& sample)
{ return central_moment<3>(sample) / std::pow(central_moment<2>(sample), 1.5); }

template<class C>
double
kurtosis(const C& sample)
{ return central_moment<4>(sample) / std::pow(central_moment<2>(sample), 2); }

template<class C>
double
raw_kurtosis(const C& sample)
{ return raw_moment<4>(sample) / std::pow(raw_moment<2>(sample), 2); }

class mean_error
{
  public:
  double mean;
  double error;
};

inline double
error_add_rule(const double x, const double y)
{ return std::sqrt(x * x + y * y); }

inline std::ostream&
operator<<(std::ostream& output, const mean_error& u)
{
  output << u.mean << "\t" << u.error;
  return output;
}

inline mean_error
operator+(const mean_error& x, const mean_error& y) noexcept
{ 
  mean_error z;
  z.mean = x.mean + y.mean;
  z.error = error_add_rule(x.error, y.error);
  return z;
}

inline mean_error
operator-(const mean_error& x, const mean_error& y) noexcept
{ 
  mean_error z; 
  z.mean = x.mean - y.mean;
  z.error = error_add_rule(x.error, y.error);
  return z;
}

inline mean_error
operator*(const mean_error& x, const mean_error& y) noexcept
{ 
  mean_error z; 
  z.mean = x.mean * y.mean;
  z.error = z.mean *
  error_add_rule((x.error / x.mean), (y.error / y.mean));
  return z;
}

inline mean_error
operator/(const mean_error& x, const mean_error& y) noexcept
{ 
  mean_error z;
  z.mean = x.mean / y.mean;
  z.error = z.mean *
  error_add_rule((x.error / x.mean), (y.error / y.mean));
  return z;
}

inline mean_error
operator+(const mean_error& u, const double a) noexcept
{ return mean_error{u.mean + a, u.error}; }

inline mean_error
operator+(const double a, const mean_error& u) noexcept
{ return u + a; }

inline mean_error
operator-(const mean_error& u, const double a) noexcept
{ return mean_error{u.mean - a, u.error}; }

inline mean_error
operator-(const double a, const mean_error& u) noexcept
{ return mean_error{a - u.mean, u.error}; }

inline mean_error
operator*(const mean_error& u, const double a) noexcept
{ return mean_error{u.mean * a, u.error * a}; }

inline mean_error
operator*(const double a, const mean_error& u) noexcept
{ return u * a; }

inline mean_error
operator/(const mean_error& u, const double a) noexcept
{ return mean_error{u.mean / a, u.error / a}; }

inline mean_error
operator/(const double a, const mean_error& u) noexcept
{ return mean_error{a, 0.0} / u; }

template <class C>
double
standard_error(const C& sample)
{ return standard_deviation(sample) / std::sqrt(1. * sample.size()); }

template <class C>
mean_error
standard_mean_error(const C& sample)
{ 
  mean_error z;
  z.mean = mean(sample);
  z.error = standard_error(sample);
  return z;
}

template <class C>
double
jackknife_error(const C& sample)
{
  double sum = 0.;
  double full_mean = mean(sample);
  for (auto it = sample.begin(); it != sample.end(); it++) {
    const double value_i = *it;
    const double mean_wo_i
      = ((full_mean * sample.size()) - value_i) / (sample.size() - 1.);
    sum += (mean_wo_i - full_mean) * (mean_wo_i - full_mean);
  }
  return std::sqrt(sum * (sample.size() - 1.) / (1. * sample.size()));
}

template <class C>
mean_error
jackknife_mean_error(const C& sample)
{ 
  mean_error z;
  z.mean = mean(sample);
  z.error = jackknife_error(sample);
  return z;
}

} // namespace qtools

#endif // QTOOLS_STATISTICS_H
