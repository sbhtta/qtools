#ifndef QTOOLS_HISTOGRAM_H
#define QTOOLS_HISTOGRAM_H

#include <array>
#include <vector>
#include <cassert>
#include <algorithm>
#include <iostream>

namespace qtools {

template <std::size_t N>
class histogram
{
  public:
  histogram(const std::array<double,3> range0,
            const std::array<double,3> range1 = {{0}},
            const std::array<double,3> range2 = {{0}})
  {
    int num_elems = 1;
    if (N > 0) {
      inc_[0]    = range0[2]; 
      min_[0]    = range0[0] - inc(0);
      max_[0]    = range0[1] + inc(0);
      size_[0]   = (max(0) - min(0)) / inc(0);
      num_elems *= size(0);
    }
    if (N > 1) { 
      inc_[1]     = range1[2];
      min_[1]     = range1[0] - inc(1);
      max_[1]     = range1[1] + inc(1);
      size_[1]    = (max(1) - min(1)) / inc(1);
      num_elems  *= size(1);
    }
    if (N > 2) { 
      inc_[2]     = range2[2];
      min_[2]     = range2[0] - inc(2);
      max_[2]     = range2[1] + inc(2);
      size_[2]    = (max(2) - min(2)) / inc(2);
      num_elems  *= size(2);
    }
    elems_.resize(num_elems);
    clear();
  }

  double
  min(const int n) const
  { return min_.at(n); }

  double
  max(const int n) const
  { return max_.at(n); }

  double
  inc(const int n) const
  { return inc_.at(n); }

  int
  size(const int n) const
  { return size_.at(n); }

  double
  coordinate(const int i) const
  {
    assert(N > 0);
    assert((0 <= i) && (i < size(0)));
    return (min(0) + (i + 0.5) * inc(0));
  }

  double
  coordinate(const int n, const int i) const
  {
    assert(n < (int)N);
    assert((0 <= i) && (i < size(n)));
    return (min(n) + (i + 0.5) * inc(n));
  }

  int
  count() const
  { return accumulate_count_; }

  void
  clear()
  {
    accumulate_count_ = 0;
    std::fill(elems_.begin(),elems_.end(),0);
    return;
  }

  double
  operator()(const int i0, const int i1 = 0, const int i2 = 0) const
  {
    if (N > 0) { assert((0 <= i0) && (i0 < size(0))); }
    if (N > 1) { assert((0 <= i1) && (i1 < size(1))); }
    if (N > 2) { assert((0 <= i2) && (i2 < size(2))); }
    return elems_[index(i0,i1,i2)] / (1. * accumulate_count_);
  }

  void
  add_data(const double x0, const double x1 = 0, const double x2 = 0)
  {
    if (N > 0) { if(!((min(0) < x0) && (x0 < max(0)))) { return; }; }
    if (N > 1) { if(!((min(1) < x1) && (x1 < max(1)))) { return; }; }
    if (N > 2) { if(!((min(2) < x2) && (x2 < max(2)))) { return; }; }
    elems_[index(scale(0,x0), scale(1,x1), scale(2,x2))] += 1;
    accumulate_count_ += 1;
    return;
  }

  void
  add_value_data(const double value, const double x0, const double x1 = 0, const double x2 = 0)
  {
    if (N > 0) { if(!((min(0) < x0) && (x0 < max(0)))) { return; }; }
    if (N > 1) { if(!((min(1) < x1) && (x1 < max(1)))) { return; }; }
    if (N > 2) { if(!((min(2) < x2) && (x2 < max(2)))) { return; }; }
    elems_[index(scale(0,x0), scale(1,x1), scale(2,x2))] += value;
    accumulate_count_ += 1;
    return;
  }

  friend std::ostream&
  operator<<(std::ostream& output, const histogram<N>& h)
  {
    if (N == 1) {
      for (int i0 = 0; i0 < h.size(0); i0++) {
        output << h.coordinate(0, i0) << "\t";
        output << h(i0) << std::endl;
      }
    } else if (N == 2) {
      for (int i1 = 0; i1 < h.size(1); i1++) {
        for (int i0 = 0; i0 < h.size(0); i0++) {
          output << h.coordinate(0, i0) << "\t";
          output << h.coordinate(1, i1) << "\t";
          output << h(i0,i1) << std::endl;
        }
      }
    } else if (N == 3) {
      for (int i2 = 0; i2 < h.size(2); i2++) {
        for (int i1 = 0; i1 < h.size(1); i1++) {
          for (int i0 = 0; i0 < h.size(0); i0++) {
            output << h.coordinate(0, i0) << "\t";
            output << h.coordinate(1, i1) << "\t";
            output << h.coordinate(2, i2) << "\t";
            output << h(i0,i1,i2) << std::endl;
          }
        }
      }
    }
    return output;
  }

  private: 
  int
  scale(const int n, const double x) const
  {
    if (n < static_cast<int>(N)) return (x - min(n)) / inc(n);
    else                         return 0;
  }

  int
  index(const int i0, const int i1 = 0, const int i2 = 0) const
  {
    switch(N) {
    case 1  : return (i0);
    case 2  : return (i0 + size(0) * i1);
    case 3  : return (i0 + size(0) * (i1 + size(1) * i2));
    default : assert(false);
    }
  }

  int accumulate_count_ = 0;
  std::array<double,N> min_{{0}};
  std::array<double,N> max_{{0}};
  std::array<double,N> inc_{{0}};
  std::array<int,N>    size_{{0}};
  std::vector<double>  elems_;
};

} // namespace qtools

#endif // QTOOLS_HISTOGRAM_H
